# MIT License

# Copyright (c) 2020 Tobias Frejo

# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:

# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.

# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.

import feedparser, re, datetime, pytz
from feedgen.feed import FeedGenerator

RSS_NV = "http://feeds.nightvalepresents.com/welcometonightvalepodcast"
RSS_GM = "http://feeds.nightvalepresents.com/goodmorningnightvale"

NV = feedparser.parse(RSS_NV)
GM = feedparser.parse(RSS_GM)

fg = FeedGenerator()
fg.load_extension('podcast')

fg.id('3b09-83fb-b9a1-3ad6-9d2f-97b6-0499-87da') # Random string
fg.author([{'name':'Night Vale Presents','email':'info@welcometonightvale.com'},{'name':'Tobias Frejo'}])
fg.title('Welcome To Good Morning Night Vale')
fg.subtitle('A combined feed of Welcome To Night Vale and Good Morning Night Vale')
fg.link(href='http://www.nightvalepresents.com/')
fg.podcast.itunes_image("https://dl.frejo.dk/file/frejodk-public/nightvalemorning/WTGMNV.jpg")


blacklist = ['Start here!', 'Trailer', '(R)', 'Bonus Episode', 'Teaser', "Alice Isn't Dead Ep", 'Bonus - ', 'Bonus: ', 'A message from Joseph Fink', 'A Spy in the Desert', 'A Big Special New Year\'s Day Announcement', 'News about books and tours', 'The Summer of Night Vale Presents', 'The Orbiting Human Circus (of the Air)', 'Within the Wires', 'Conversations with People Who Hate Me', 'I Only Listen to the Mountain Goats', 'It Makes A Sound', 'Pounded in the Butt by My Own Podcast', 'Good Morning Night Vale: Good Morning Pilot', 'Adventures in New America', 'Dreamboy, Episode One', 'Start With This: Idea to Execution', 'The First Ten Years', 'You Feel It Just Below the Ribs', 'Announcement:']
gmReplaceStrings = {
    'Old Oak Doors (Live from PodX)' : 'Old Oak Doors',
    'The Visitor' : 'Visitor',
    'Walk' : 'WALK',
    'Auction' : 'The Auction',
    'Memories of Europe' : 'A Memory of Europe',
    'A Story About You' : 'A Story About You.',
    'Briny Depths' : 'BRINY DEPTHS',
}
def getTitles(feed):
    lTitles = []
    for item in feed['entries']:
        title = item['title']
        if not any(word in title for word in blacklist):
            lTitles.append(title)
    return lTitles

def matchEpisodes():
    matches = []
    nvEpisodes = getTitles(NV)
    gmEpisodes = getTitles(GM)
    for gmEpisode in gmEpisodes:
        epMatches = []
        gmEpisodeNoGoodmorning = gmEpisode.replace('Good Morning ','')
        matchcount = 0
        for nvEpisode in nvEpisodes:
            try:
                nvEpisodeNoNumber = nvEpisode.split(' - ', 1)[1]
            except:
                nvEpisodeNoNumber = nvEpisode
            if gmEpisodeNoGoodmorning == nvEpisodeNoNumber:
                matchcount = matchcount + 1
                epMatches.append(
                    {'nightvale': nvEpisode, 'goodmorningnightvale': gmEpisode}
                )

        # Try other more less strict tests, if the first didn't succeed
        if matchcount < 1:
            for nvEpisode in nvEpisodes:
                try:
                    nvEpisodeNoNumber = nvEpisode.split(' - ', 1)[1]
                except:
                    nvEpisodeNoNumber = nvEpisode
                if gmEpisodeNoGoodmorning in nvEpisodeNoNumber:
                    matchcount = matchcount + 1
                    epMatches.append(
                        {'nightvale': nvEpisode, 'goodmorningnightvale': gmEpisode}
                    )
                elif gmEpisodeNoGoodmorning in gmReplaceStrings:
                    if gmReplaceStrings[gmEpisodeNoGoodmorning] in nvEpisodeNoNumber:
                        matchcount = matchcount + 1
                        epMatches.append(
                            {'nightvale': nvEpisode, 'goodmorningnightvale': gmEpisode}
                        )
                else:
                    # Test if Good Morning episode has a multipart episode number (eg. (19A)) in the title, and match with Night Vale episode number
                    try:
                        multipartEpRegEx = r'\(([0-9]*[A-Z])\)'
                        reResult = re.search(multipartEpRegEx, gmEpisodeNoGoodmorning)
                        partedEpNumber = reResult.group(1)
                        if partedEpNumber == nvEpisode.split(' - ', 1)[0]:
                            matchcount = matchcount + 1
                            epMatches.append(
                                {'nightvale': nvEpisode, 'goodmorningnightvale': gmEpisode}
                            )
                    except:
                        pass


        if matchcount > 1:
            print("%s matches for %s" % (matchcount, gmEpisode))
            if all(item['nightvale'].split(' - ', 1)[0] == epMatches[0]['nightvale'].split(' - ', 1)[0] for item in epMatches):
                print('%s is multipart on main feed' % gmEpisode)
                for item in epMatches:
                    newItem = {}
                    for name,title in item.items():
                        newItem[name] = title
                    newItem['isMultipart'] = True
                    matches.append(newItem)
        elif matchcount < 1:
            print("Couldn't match %s" % gmEpisode)
        else:
            matches.append(epMatches[0])
    return matches

def nvEpNum(title):
    # Get the episode number of a Night Vale episode title. A title is formatted like "1 - Pilot"
    # Ep 124-127 uses '–' (en dash) instead of '-' (hyphen)
    if ' - ' in title:
        nvNum = title.split(' - ', 1)[0]
    elif ' – ' in title:
        nvNum = title.split(' – ', 1)[0]
    else:
        nvNum = None
    if nvNum == '49':
        # Episode 49 Old Oak Doors is split into two with the same number, so append A/B from the title ending in "Part X"
        nvNum = nvNum + title[len(title)-1]
    return nvNum

def gmEpNum(title, description):
    # Get the episode number of the corresponding Night Vale episode from the description of a Good Morning Night Vale episode. 
    # A description is usually "Meg, Symphony and Hal discuss episode X of Welcome to Night Vale: EPISODE TITLE." + some exxtra stuff. An exepction is the pilot. 
    if 'discuss episode ' in description and ' of Welcome to Night Vale' in description:
        gmNum = description.split('discuss episode ')[1].split(' of Welcome to Night Vale')[0]
    elif 'discuss the pilot episode of Welcome to Night Vale' in description:
        # A special rule for the pilot not following the layout of all the other episodes
        gmNum = '1'
    elif 'Welcome to Night Vale: Old Oak Doors' in description:
        # A special rule for Good Morning Old Oak Doors (Live from PodX)
        gmNum = '49B'
    elif title == "Good Morning Who's a Good Boy? Part 1":
        gmNum = '89'
    elif title == "Good Morning The 12:37":
        gmNum = '91'
    else:
        gmNum = None
    return gmNum

def scanEpisodes():
    # Bring all episodes into a dict using episode number as index, while excluding blacklisted episodes
    nightvale = {}
    goodmorning = {}
    for e in NV.entries:
        if not any(word in e.title for word in blacklist):
            ep = nvEpNum(e.title)
            if ep:
                nightvale[ep] = e
            else:
                print("Error: %s" % e.title)
        else:
            print("Blacklisted: %s" % e.title)
    for e in GM.entries:
        if not any(word in e.title for word in blacklist):
            ep = gmEpNum(e.title, e.description)
            if ep:
                goodmorning[ep] = e
            elif 'Live Show' in e.description:
                print("Info: %s is a live show" % e.title)
            else:
                print("Error: %s" % e.title)
        else:
            print("Blacklisted: %s" % e.title)
    return nightvale, goodmorning

def numMatchEpisodes(nvEpisodes, gmEpisodes):
    # match episodes based on episode number
    matches = {}
    for ep in gmEpisodes:
        if ep in nvEpisodes:
            matches[ep] = {'nightvale': nvEpisodes[ep], 'goodmorningnightvale': gmEpisodes[ep]}
            pass
    
    # manual additions (for episodes without a match)
    matches['49A'] = {'nightvale': nvEpisodes['49A'], 'goodmorningnightvale': None}
    return matches

def getEpisodeDetails(title, podcast):
    for episode in podcast.entries:
        if title == episode.title:
            link = episode.link
            desc = episode.description
            subtitle = episode.subtitle
            guid = episode.id
            date = pytz.timezone("UTC").localize(datetime.datetime(*episode.published_parsed[0:6]))
            try:
                author = episode.author
            except:
                try:
                    author = podcast.feed.author
                except:
                    author = "Unkown Author"
            try:
                image = episode.image
            except:
                try:
                    image = podcast.feed.image
                except:
                    image = None
            media = None
            for l in episode.links:
                if l.type == 'audio/mpeg':
                    media = l
    return link, desc, author, guid, date, image, media


def createCombinedFeed(matches):
    for match in matches:
        # Create nightvale entry
        link, desc, author, guid, nvDate, image, media = getEpisodeDetails(match['nightvale'], NV)
        eg = fg.add_entry()
        eg.title(match['nightvale'])
        eg.link({'href':link,'rel':'alternate'})
        eg.description(desc)
        eg.author({'name':author})
        eg.id(guid)
        eg.published(nvDate)
        eg.link(media)
        eg.podcast.itunes_image(image.href)
        
        # Create goodmorning entry with nightvale date + 10 minutes
        link, desc, author, guid, date, image, media = getEpisodeDetails(match['goodmorningnightvale'], GM)
        eg = fg.add_entry()
        eg.title(match['goodmorningnightvale'] + " (%s)" % date)
        eg.link({'href':link,'rel':'alternate'})
        eg.description(desc)
        eg.author({'name':author})
        eg.id(guid)
        eg.published(nvDate + datetime.timedelta(minutes=10))
        eg.link(media)
        eg.podcast.itunes_image(image.href)
    fg.atom_file('feed/atom.xml')
    fg.rss_file('feed/rss.xml')

def newCombinedFeed():
    nvEpisodes, gmEpisodes = scanEpisodes()
    matches = numMatchEpisodes(nvEpisodes, gmEpisodes)
    for ep in matches:
        if matches[ep]['nightvale']:
            # Create nightvale entry
            link, desc, author, guid, nvDate, image, media = getEpisodeDetails(nvEpisodes[ep].title, NV)
            eg = fg.add_entry()
            eg.title(nvEpisodes[ep].title)
            eg.link({'href':link,'rel':'alternate'})
            eg.description(desc)
            eg.author({'name':author})
            eg.id(guid)
            eg.published(nvDate)
            eg.link(media)
            eg.podcast.itunes_image(image.href)
        
        if matches[ep]['goodmorningnightvale']:
            # Create goodmorning entry with nightvale date + 10 minutes
            link, desc, author, guid, date, image, media = getEpisodeDetails(gmEpisodes[ep].title, GM)
            eg = fg.add_entry()
            eg.title(gmEpisodes[ep].title + " (%s)" % date)
            eg.link({'href':link,'rel':'alternate'})
            eg.description(desc)
            eg.author({'name':author})
            eg.id(guid)
            eg.published(nvDate + datetime.timedelta(minutes=10))
            eg.link(media)
            eg.podcast.itunes_image(image.href)
    fg.atom_file('feed/atom.xml')
    fg.rss_file('feed/rss.xml')

def printlist(l):
    for i in l:
        print(i)


def __test__sort__():
    n, g = scanEpisodes()
    print('Night Vale Episodes:')
    for e in n: print(e)
    print('Good Morning Episodes:')
    for e in g: print(e)
    for e in g:
        if e in n:
            print('%s :::: %s :::: %s' % (e, n[e]['title'], g[e]['title'] ))

if __name__ == "__main__":
    # printlist(getTitles(NV))
    # printlist(getTitles(GM))
    # printlist(matchEpisodes())
    #createCombinedFeed(matchEpisodes())
    newCombinedFeed()
    pass
